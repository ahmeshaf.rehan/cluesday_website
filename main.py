
from flask import Flask
import json
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify, Response
from sqlalchemy import types, DateTime
from datetime import datetime
from flask import render_template

app = Flask(__name__, static_url_path='')
db = SQLAlchemy(app)

class User(db.Model):
    date_joined = db.Column(DateTime, default=datetime.now())
    phone_no = db.Column(db.Integer, unique=True)
    email_id = db.Column(db.String(100), primary_key=True, unique=True)
    name = db.Column(db.String(100))
    password = db.Column(db.String(128))

    def __init__(self, name, email, password):
        self.password = password
        self.email_id = email
        self.name = name

    #def __init__(form_dict):
       # self.name = form_dict['firstName']
       # self.email_id = form_dict['username']
       # password = form_dict['password']

    def getdict(self):
        import calendar;
        return {
                "date_joined": calendar.timegm(self.date_joined.timetuple()),
                "phone_no": self.phone_no,
                "username": self.email_id,
                "firstName": self.name
                }


class QuizRoster(db.Model):
    edition = db.Column(db.Integer, primary_key=True, autoincrement=True)
    quiz_date = db.Column(DateTime)
    member_phn_id = db.Column(db.Integer)
    slides_path = db.Column(db.String(100))

    def __init__(self, edition, date, mem_phn):
        self.edition = edition
        self.quzi_date = date
        self.member_phn_id = mem_phn

def wrap_model(model):
    class_name_lower = model.__class__.__name__.lower()

    table_col_list = [col for col in model.__dict__.keys() if not
            col.startswith('_')]

    # Create
    #create_route = 'create_'
    #@app.route('/create_entry/<email>/<phone>/<access_key>')


    # get by any key

    # get all

    # update by a key

    # TODO: update using any condition

    # delete by a key

    # delete all

    pass

app.config['SQLALCHEMY_DATABASE_URI'] ='mysql://user:user123@localhost/cluesdaydb'
# db = SQLAlchemy(app)
db.create_all()
db.session.commit()

# mem1 = User(126, 'lola2@foo.com', 'dummy1')
# mem2 = User(1257, 'lola4@foo.com', 'dummy2')

# db.session.add(mem1)
# db.session.add(mem2)
# db.session.commit()

@app.route('/create_entry/<email>/<phone>/<access_key>')
def create_entry(email, phone, access_key):
    # user = User(email, phone, access_key)
    # db.session.add(user)
    # db.session.commit()
    return 'ok'

@app.route('/update_entry_by_email/<email>/<phone>/<access_key>')
def update_entry_by_email(email, phone, access_key):
    user = User.query.filter_by(email=email).first()
    if phone:
        user.phone = phone
    if access_key:
        user.access_key = access_key
    db.session.commit()
    return 'ok'

@app.route('/update_entry_by_phone/<phone>/<email>/<access_key>')
def update_entry_by_phone(phone, email, access_key):
    user = User.query.filter_by(phone=phone)
    if email:
        user.email = email
    if access_key:
        user.access_key = access_key
    db.session.commit()
    return 'ok'

@app.route('/get_by_phone/<phone>')
def get_by_phone(phone):
    user = User.query.filter_by(phone=phone).first()
    return user.getdict()

@app.route('/get_by_email/<email>')
def get_by_eail(email):
    user = User.query.filter_by(email=email).first()
    return user.getdict()

@app.route('/strava/token_exchange')
def get_code():
    # return "hello"
    return "copy this : " + request.args['code']

@app.route("/create_name/<name>")
def create_name(name):
    user = User(name)
    db.session.add(user)
    db.session.commit()
    return 'ok'

@app.route('/get_by_name/<name>')
def get_by_name(name):
    user = User.query.filter_by(name=name).first()
    return user.getdict()

@app.route('/get_all/')
def get_all():
    # import pdb; pdb.set_trace()
    users = User.query.all()
    return str(jsonify(results=users))
    # return user.getdict()

succes_dict = {'success':True}
fail_dict = {'success':False, 'message': 'Username or password is incorrect'}

@app.route('/api/authenticate', methods = ['GET', 'POST'])
def authenticate():
    form_data = request.get_json()
    # print form_data
    # import pdb; pdb.set_trace()
    username = form_data['username']
    password = form_data['password']
    user = User.query.filter_by(email_id=username).first()
    data = {}
    if user is None:
        fail_dict['message'] = 'Username not found!'
        data = fail_dict
    elif user.password != password:
        fail_dict['message'] = 'Invalid Password!'
        data = fail_dict
    else:
        data = succes_dict

    return ret_response(data)

@app.route('/api/users', methods = ['GET'])
def get_all_users():
    users = User.query.all()
    users_arr = []
    for user in users:
        users_arr.append(user.getdict())
    print str(users_arr)
    resp = Response(response=json.dumps(users_arr),
            status=200,
            mimetype="application/json")
    return resp

@app.route('/api/users/<email>')
def get_by_email(email):
    print 'get email: ' + email
    user = User.query.filter_by(email_id=email).first()
    resp = Response(response=json.dumps(user.getdict()),
            status=200,
            mimetype="application/json")
    return resp

@app.route('/api/users', methods = ['POST'])
def create_user():
    # import pdb; pdb.set_trace()
    user_data = request.get_json()
    print user_data
    email = user_data['username']
    user = User.query.filter_by(email_id=email).first()
    if user is not None:
        fail_dict['message'] = 'Username ' + email + ' already taken'
        return ret_response(fail_dict)
    user = User(user_data['firstName'], user_data['username'],
            user_data['password'])
    db.session.add(user)
    db.session.commit()

    return ret_response(succes_dict)

def ret_response(json_obj):
    resp = Response(response=json.dumps(json_obj),
            status=200,
            mimetype="application/json")
    return resp

#@app.route("/")
#def root():
#    # print('request args')
#    # print(request.args)
#    # print(request.form)
#    return 'hello'

@app.route("/")
def run_index():
    # import pdb; pdb.set_trace()
    # print('request args')
    # print(request.args)
    # print(request.form)
    return app.send_static_file('index.html')
    # return render_template("index.html")
    # return 'hello, mr. anderson'

if __name__ == "__main__":
    app.debug = True
    app.run(host='127.0.0.1',port=8080)
